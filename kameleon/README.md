About
=====

This directory contains the recipe to built the operating system to be used in this tutorial (IA stack included).
The recipe is to be used with the kameleon tool (http://kameleon.imag.fr). It builds a new Grid'5000 kadeploy environment, customized for the need.

Recipe details
==============

This recipe extends a kameleon recipe which allows creating a new kadeploy environment based on an existing other kadeploy environment (see in the kameleon-recipes/from_kadeploy_environment/ directory). Here we customize the `debian-x64-big` Grid'5000 environment to add the IA stack.

The recipe adds some steps (defined in the steps directory) in its setup section, in order to install miniconda system-wide, and then the following conda packages:
- ipython
- pytorch

How to build
============

First reserve a node where to run the build:

```
frontend$ oarsub "sleep 2h" -l nodes=1,walltime=2
```

Once the job is running, connect to your node and install the required components:

```
node$ pip2 install --user execo
node$ gem install --user-install kameleon-builder
```

Make the `kameleon` command reachable with the PATH environment variable:

```
node$ export PATH=$HOME/.gem/ruby/2.3.0/bin:$PATH
```

Then go to the kameleon directory (the directory which contains this file) and start the **build**:

```
node$ kameleon build debian9-x64-ia-tutorial
```
